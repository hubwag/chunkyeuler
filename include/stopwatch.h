/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once
#include <chrono>

struct Stopwatch {
  std::chrono::time_point<std::chrono::high_resolution_clock> last;
  Stopwatch() {
    last = std::chrono::high_resolution_clock::now();
  }

  double lap() {
    auto now = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::duration<double>>(
      now - last).count();
    last = now;
    return duration;
  }
};

