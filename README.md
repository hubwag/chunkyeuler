# Documentation 

## Overview
* Chunky Euler computes the Euler characteristic curves of images.
* Euler characteristic curve can be used as a (topological) descriptor of the image.
* Input is a grayscale image in 2,3 or higher dimension.
* The software accepts only 'raw' binary files, but we provide conversion tools.
* The computations are efficient, and done in a streaming and  parallel way; we can handle terabyte scale images (10'000 x 10'000 x 10'000 and more) on normal hardware.

## Introduction
Chunky Euler is an efficient software to compute Euler characteristic curves of grayscale images of arbitrary dimension. 

The Euler characteristic curve is used in various applications as a descriptor of an image. However, many interesting three-dimensional (or higher dimensional) images, for example CT scans, are too large to fit in main memory. Our software addresses this problem by performing computation in a streaming manner, without storing the entire image in memory.

Chunky Euler is the first streaming software for Euler characteristic curves. Experiments show that it handles terabyte scale images on commodity hardware. Due to lock-free parallelism, it scales well with the number of processor cores.

The Euler characteristic curve of an image is (the graph of) the function that maps each grayscale value to the Euler characteristic of the thresholding at this value. The Euler characteristic captures the topological information at each level -- namely information about the connected components and other types of holes. For the mathematically inclined, the whole procedure captures some information about the evolution of homology groups for consecutive sub-level sets of the real-valued function represented by the input image.

![Euler characteristic curve](docs/images/euler.gif "Upper: monochrome thresholded image. Lower: Euler characteristic curve")

For more details, see [https://arxiv.org/abs/1705.02045](https://arxiv.org/abs/1705.02045 "https://arxiv.org/abs/1705.02045").

This manual focuses on handling three-dimensional images, which is likely the most popular use case. However, in Section Other Dimensions we describe how images in other dimensions can be handled.

## Installation 

The main program is a simple to use command-line utility. While it's easy to compile using cmake (3.1 and above) and a modern C++ compiler (g++ 4.9 and above or clang++ 3.9 and above), we provide a statically-linked 64bit Windows binary configured to work in 3D. This program can be found in Downloads ([https://bitbucket.org/hubwag/chunkyeuler/downloads/](https://bitbucket.org/hubwag/chunkyeuler/downloads/ "https://bitbucket.org/hubwag/chunkyeuler/downloads/")), and should work on modern 64 bit Windows systems.

Compiling 64bit executables is encouraged. Note that 32bit executables will not be able to handle large files.

### Linux/Unix

Make sure you have recent cmake installed.

`cd <path to empty folder>`

`git clone https://bitbucket.org/hubwag/chunkyeuler.git` downloads source code from repository to an empty folder

`cd chunkyeuler`

`mkdir build`

`cd build`

`cmake .. -DCMAKE_BUILD_TYPE=Release` creates the build files (usually a unix makefile)

If you want to use it for images in dimension other than 3, replace this line by: 

`cmake .. -DCONFIG_INPUT_DIM=<desired dimension>  -DCMAKE_BUILD_TYPE=Release`

`cmake  --build  . --config Release` compiles the software, the executable is placed in the current directory. 

### Windows

Make sure you have recent cmake and visual studio installed (it's also possible to use mingw).

`cd <path to empty folder>`

`git clone https://bitbucket.org/hubwag/chunkyeuler.git` downloads source code from repository to an empty folder

`cd chunkyeuler`

`mkdir build`

`cd build`

`cmake .. -DCMAKE_GENERATOR_PLATFORM=x64` creates the build files (usually Visual Studio solution)

If you want to use it for images in dimension other than 3, replace this line by: 

`cmake .. -DCMAKE_GENERATOR_PLATFORM=x64 -DCONFIG_INPUT_DIM=<desired dimension>`

`cmake  --build  . --config Release` compiles the software, the executable is placed in directory `Release`.

## Basic usage
`./CHUNKYEuler -f ../example_data/neghip_t_u8_64x64x64.raw -t u8 -s "64 64 64"` 

Note the quotes surrounding the size! This can be done simpler, since the size is encoded in the filename:

`./CHUNKYEuler -f ../example_data/neghip_t_u8_64x64x64.raw`

Either of these lines create file `../example_data/neghip_t_u8_64x64x64.raw.euler` which is a text file containing the resulting Euler characteristic curve. 

Note Advanced Usage section for details about embedding size and voxel encoding inside the image filename.

Afterwards, you can use `python ../scripts/plotEuler.py ../example_data/neghip_t_u8_64x64x64.raw.euler` to plot the resulting Euler characteristic curve.

## Usage
`./CHUNKYEuler -f <path to the input image> -s "<size1> <size2> <size3>" [-w <width>] [-t <type>] [-c <num_chunks>] [-p <num_threads>]`
with
1. `-f <path to the input image>` (required)
  path to a three-dimensional grayscale image, as a binary file. (More information, see Section Input Image Format.)
  The Euler characteristic curve of this image will be computed.
  
2. `-s "<size1> <size2> <size3>"` (required if none of the other ways to specify the size were used (see Section Advanced Usage))
  three integers specifying the size of the image
  (e.g., `-s "10000 5000 20000"` for a 10000 x 5000 x 20000 image)
  
3. `-w <width>` (optional, alternative to -s)
  an integer,
  `-w <width>` is equivalent to `-s "<width> <width> <width>"`
  (Do not use -w together with -s.)
  
4. `-t <type>` (optional, default = u8 (unsigned 8-bit integer))
  one of the following options: u8,s8,u16,s16,u32,s32 specifying the integer data type in which the gray values of the input image are encoded.
  u/s stands for unsigned/signed. And the number next to it gives the number of bits used to encode one integer.
  
5. `-c <num_chunks>` (optional, default = the number of chunks needed to cut the image in slices that are each 4 voxels thick (except boundary effects))
  an integer specifying the number of chunks the image should be cut into. The image will be sliced along to the first axis. Use many chunks to save memory.
  
6. `-p <num_threads>` (optional, default = number of physical cores)
  an integer specifying the number of worker threads used for the computations. By default, all cores will be utilized, decrease the number if you need to use the computer for other things...

## Input Image Format
The format for the input image is a binary 'raw' file where the grayscale values of the voxels are stored linearly (as in a C array). In other words, the values are stored as if the indices/coordinates of the voxels were sorted lexicographically. 
Note that this is not consistent with how sizes of 2D images are specified.
Each value may be encoded as a 8, 6 or 32 bit signed or unsigned integer. The encoding is specified with the `-t <type>` option. 

Python scripts for converting images and stacks of images to raw files (and back) are provided in folder `scripts` (They work for Python 2 and Python 3). Also, scripts for conversions between perseus format and raw format are provided in folder `scripts` (They work for Python 2 only).

3D raw images can be visualized for example with ImageVis3D: [link to ImageVis3D](http://www.sci.utah.edu/software/imagevis3d.html)

### Convert a Stack of PNG/JPEG Images into a 3D raw File
First create a folder `<path to image folder>` and put PNG/JPEG images inside where every image resembles a slice of a 3D image and where the order of the slices is the alphabetical order of their file names. We recommend giving the images the suffix 000000, 000001, 000002, ... to ensure the correct alphabetical order. The python script `image_stack_to_raw.py` will stack the files in alphabetic order on top of each other to create a 3D raw image that is u8 encoded (unsigned 8-bit-integer). For this, type `python <path>/chunkyeuler/scripts/image_stack_to_raw.py <path to image folder>/*.<ext>` where `<ext>` is the extension of the files (png or jpg). The output will be written with suffix `_t_u8_<size>.from_stack.raw`.

### Convert a 2D PNG/JPEG Image into a 2D raw File
The procedure described in the above section (Convert a Stack of PNG/JPEG Images into a 3D raw File) works also for 'stacks' consisting of one single image. In this case the output is a 1 x height x width raw 3D image, which is the same as a height x width raw 2D image.

## Output Format for the Euler Characteristic Curve
Chunky Euler creates a new file `<path to the input image>.euler` for the output:

The output file contains the sorted list of points (g\_{i},c\_{i}) where the Euler characteristic curve changes. Between the gray values g\_{i} and g\_{i+1} the Euler characteristic curve has the constant value c\_{i}, and at g\_{i+1} it jumps to c\_{i+1}.

To plot the Euler characteristic curve, use the python script `plotEuler.py` by typing`python <path>/chunkyeuler/scripts/plotEuler.py <path to the input image>.euler`. It requires  matplotlib, numpy and sys. The python script plots the points (g\_{i},c\_{i}) and attaches horizontal lines to the right of each plotted point (g\_{i},c\_{i}) that end at g\_{i+1}.
If you wish to instead plot straight lines connecting the points, use the additional parameter `"c"` (c for continuous): `python <path>/chunkyeuler/scripts/plotEuler.py <path to the input image>.euler "c"`. Be aware that the continuous version is just a way to visualize the points (g\_{i},c\_{i}) and it does not make sense to linearly interpolate between the points because the Euler characteristic curve is a discrete function.

![Explanation output format](docs/images/small_example_t_u8_5x3x2.raw.GOLDEN.euler.plot.png "Construction of the graph of the Euler characteristic curve. You get this plot by running Chunky Euler on the three-dimensional example image small_example_u8_5x3x2.raw and plugging the output into the python script plotEuler.py")

## Advanced Usage

### Specifying the Size of the Input Image
There are four different ways to specify the size of the input image:
1. If the filename of the input image contains `<size1>x<size2>x<size3>` the size of the image will be set to <size1> x <size2> x <size3>.
2. If the size is not already specified with the first option, you can specify it with `-s "<size1> <size2> <size3>"`.
3. If the size is not already specified with one of the first two options, you can specify the size with `-w <width>`.
4. If the size is not already specified differently, Chunky Euler sets the size to the size specified in the header if the input file has a header. We say a file has a header, if before listing all the gray values, the 3 integers specifying the size <size1> <size2> <size3> of the image are listed. (Chunky Euler finds out if the file has a header or not, by reading the first three numbers (or the first d numbers if another dimension d is used) of the input file and checking if the product of them equals the number of remaining numbers in the file.)

If none of the four options is used to specify the size, the following error message will appear: `File Header not found or inconsistent with file length. Will need to determine dimensions differently.
dimensions of file were not specified (using -w or -s) and could not be read from file header. Quitting...`

### Specifying the Voxel Value Type (Encoding)
There are two different ways to specify the voxel value type:
1. If the filename of the input image contains `_t_<type>_` with <type> being one of the options u8,s8,u16,s16,u32,s32, the integer data type will be set to <type>.
2. If the integer data type is not already specified with the first option, you can specify it with `-t <type>`.

The default value is u8 (unsigned 8-bit integer).

## Other Dimensions

### Recompile for Different Dimensions (2D, 4D, ...)
The default configuration works for the dimension is 3. To change the dimension you need to pass a parameter to cmake like this:
 `cmake .. -DCONFIG_INPUT_DIM=<desired dimension>` (alternatively, if you're playing with the code, you can change the `embedding_dim` constant in `include/user_config.h`). Note that the resulting executable will only work for the chosen dimension, so you may need to configure and compile multiple times.

All the options work analogously: for example, specify size for each dimension `-s "<size1> <size2> ... <size d>" `; `-w <width>` is now equivalent to `-s "<width> <width> ... <width>"`.

### (Ab)use the Standard 3D Version for 2D Images
To use two-dimensional images, you have two options: Either recompile Chunky Euler for 2D (see section above: Recompile for Different Dimensions (2D, 4D, ...)) or you use the standard three-dimensional version for two-dimensional images, by specifying the size along the third dimension to be 1:
`./CHUNKYEuler -f ../example_data/Planck_t_u8_500x1000.raw -s "500 1000 1" -c 4 -p 4` computes the Euler characteristic curve of the two-dimensional 500x1000 image. 

Small Remark: Note that the output is independent of the position of the 1 (as long as the order of height and width is preserved). In other words, `-s "1 500 1000"`, `-s "500 1 1000"` and `-s "500 1000 1"` will yield exactly the same Euler characteristic curve (because 500 comes before 1000 in all of them). However, in the case of `-s "1 500 1000"` the chunking and parallelism are effectively disabled, which is a problem for large 2D files. We therefore recommend putting the 1 in the last position. 

## Authors
Authors: Teresa Heiss (teresa.heiss@ist.ac.at) and Hubert Wagner (hubert.wagner@ist.ac.at), IST Austria

If you made some improvements to the code please let us know by making a pull-request.
If you have any questions or comments please contact us.

## License Info
License: GNU Lesser General Public License, see chunkyeuler/LICENSE.

If you publish any work that uses this software, please cite:
Heiss T., Wagner H. (2017) Streaming Algorithm for Euler Characteristic Curves of Multidimensional Images. In: Felsberg M., Heyden A., Krueger N. (eds) Computer Analysis of Images and Patterns. CAIP 2017. Lecture Notes in Computer Science, vol 10424. Springer, Cham, pp. 397-409


