/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once
#include <bitset>
#include <future>
#include <map>
#include <limits>
#include <string>
#include <sstream>
#include <fstream>
#include <regex>
#include <set>
#include <unordered_map>
#include <unordered_set>

#include <blitz/array.h>
#include <blitz/blitz.h>
#include <blitz/tinyvec-et.h>

#include "container_io.h"
#include "user_config.h"
#include "blitz_operators.h"
#include "cube_boundary_relations.h"

#include "stopwatch.h"

/* Properties of cubical complexes to be used if an input voxel is interpreted
  as a top-dimensional cubical cell (default). */
struct voxel_traits {
  static index_t to_big_coords(const index_t &small_coords) {
    return 2 * small_coords + 1;  // !!!
  }

  static index_t to_small_coords(const index_t &big_coords) {
    return (big_coords - 1) / 2;
  }

  static index_t enlarge_extent(const index_t &small_extent) {
    return small_extent * 2 + 1;
  }

  template <typename val_t, typename other_val_t>
  static bool is_this_my_face(const val_t &my_val, const other_val_t &his_val) {
    return my_val < his_val;
  }

  enum : size_t { VOXEL_TYPE_ID = (1ULL << embedding_dim) - 1 };

  static const index_t get_base() { return index_t(1); }
};

// Computes the number of voxels in a voxel grid of a given size.
size_t num_voxels(index_t sz) {
  size_t p = 1;
  for (auto x : sz) p *= x;
  return p;
}

/* Properties of cubical complexes to be used if an input voxel is interpreted
as a lowest-dimensional cubical cell, i.e. a vertes (default). */
struct vertex_traits {
  index_t to_big_coords(const index_t &small_coords) {
    return 2 * small_coords;  // !!!
  }

  static index_t to_small_coords(const index_t &big_coords) {
    return big_coords / 2;
  }

  static index_t enlarge_extent(const index_t &small_extent) {
    return small_extent * 2 - 1;
  }

  template <typename val_t, typename other_val_t>
  static bool is_this_my_face(const val_t &my_val, const other_val_t &his_val) {
    return my_val > his_val;
  }

  static const index_t get_base() { return index_t(0); }
};

/* Gets an unique id of a cell from its coords (index).
It agrees with blitz iterator ordering. */
cube_id_t get_id_with_extent(const index_t &extent, const index_t &coords) {
  cube_id_t id = 0;
  size_t slice = 1;
  for (int i = coords.length() - 1; i >= 0; i--) {
    id += slice * coords[i];
    slice *= extent[i];
  }
  return id;
}

// Gets a (multidimensional) index from cells unique id.
index_t get_index_with_extent(const index_t &extent, cube_id_t id) {
  index_t ind;

  for (int i = extent.length() - 1; i >= 0; i--) {
    ind[i] = id % extent[i];
    id /= extent[i];
  }
  return ind;
}

// Computes the type of cube. 
// E.g. in 2D we have: vertices, horizontal edges, vertical edges and squares.
cube_id_t get_cube_type(index_t ind) {
  return get_id_with_extent(index_t{ 2 }, index_t(ind % 2));
}

struct less_mask_class {
  template <typename mask_t>
  bool operator()(const mask_t &a, const mask_t &b) const {
    for (size_t i = 0; i < a.size(); i++) {
      if (a[i] < b[i]) return true;
      if (a[i] > b[i]) return false;
    }
    return false;
  }
};

// Quick integer power computation.
constexpr int64_t ipow(int64_t base, int exp, int64_t result = 1) {
  return exp < 1 ? result : ipow(base * base, exp / 2,
    (exp % 2) ? result * base : result);
}


template <int D>
struct neighbour_traits {
  using mask_t = std::bitset<ipow(3, D)>;
  using less_mask = less_mask_class;
};

template <>
struct neighbour_traits<2> {
  using mask_t = size_t;
  using less_mask = std::less<size_t>;
};

template <>
struct neighbour_traits<1> {
  using mask_t = size_t;
  using less_mask = std::less<size_t>;
};

// dim, val, id, [newid placeholder]
using cube_descriptor =
std::tuple<int8_t, function_value_t, cube_id_t, cube_id_t>;

// plain int version is 2x faster than using bitset...
template <>
struct neighbour_traits<3> {
  using mask_t = size_t;
  using less_mask = std::less<size_t>;
};

using face_number_t = char;  // up to dim 5?
using mask_t = neighbour_traits<embedding_dim>::mask_t;
using less_mask_t = neighbour_traits<embedding_dim>::less_mask;

void test_get_id() {
  index_t sz{ 5 };
  matrix_t a(sz);

  int i = 0;
  for (auto &x : a) x = i++;

  for (auto it = a.begin(); it != a.end(); ++it)
    assert(*it == get_id_with_extent(a.extent(), it.position()));
}
