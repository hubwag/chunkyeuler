"""  Copyright 2015-2018 IST Austria
    File contributed by: Teresa Heiss, Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. """

import struct
import sys

#example = 'c:/work/hydrogenAtom_128x128x128.raw'

def perseus_to_raw(filename):        
    f = open(filename, 'rt')
    
    dim = int(f.readline())
    sz = [int(f.readline()) for _ in range(dim)]
    sz.reverse()

    size_string = 'x'.join(map(str, sz))
    output_filename = filename + '_t_u8_' + size_string + '.raw'
    
    out = open(output_filename, 'wb')

    for v in f:        
        x = struct.pack('B', int(v))
        out.write(x)

    f.close()
    out.close()

    print('output succesfully written to: {}'.format(output_filename))

if __name__ == '__main__':    
    if len(sys.argv) < 2:
        print('Converts an arbitrary dimensional image file from perseus ascii format to u8 (unsigned 8-bit integer) encoded raw format.')
        print('Output is written with suffix _u8_<size>.raw')
        print('usage: python {} <perseus_input_filename>'.format(sys.argv[0]))
        print('Exitting...')
        exit(-1)
        
    perseus_to_raw(sys.argv[1])
