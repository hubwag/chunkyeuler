/*  Copyright 2015-2018 IST Austria
    File contributed by: Teresa Heiss, Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. */

#include <fstream>

#include <cxxopts.hpp>
#include <random>
#include <thread>
#include <future>
#include <vector>
#include <chrono>
#include <iostream>
#include <ostream>
#include <thread>
#include <queue>
#include <set>
#include <limits>
#include <algorithm>
#include <utility>
#include <tuple>
#include <array>
#include <cstdio>

#ifndef NOMINMAX
  #define NOMINMAX
#endif

#include "ctpl_stl.h" // CTPL thread pool

#pragma warning(push)
#pragma warning(disable: 4244) // possible loss of data
#pragma warning(disable: 4267)

#include "common.h"
#include "run_computations.h"

#pragma warning(pop)

int main(int argc, char **argv) {
  std::cerr.precision(5);
  std::cout.precision(5);  
  
  std::cout << "Compiled for images dimension = " << embedding_dim << std::endl;
  std::cout << "Compiled in " <<  8*sizeof(size_t) << "bit mode" << std::endl;

  Stopwatch sw_total;
  cxxopts::Options opts(argv[0], "(Computes the Euler Characteristic Curve of multidimensional greyscale images)");

  opts.add_options()
    ("h,help", "Display help")
    ("f,file", "Path to input file (raw voxel format)", cxxopts::value<std::string>()->default_value("NONE"))
    ("s,sizes", "Specify dimensions of 3D voxel image:  \"W0 W1 W2\"", cxxopts::value<std::string>()->default_value("0 0 0"))
    ("t,type", "Input type (voxel encoding): signed or unsigned and number of bits per voxel's function value); possible options: u8,s8,u16,s16,u32,s32 (default: u8)", cxxopts::value<std::string>()->default_value(""))
    ("w,width", "Alternative size specification: Specify the width of image, assuming size is w^3", cxxopts::value<size_t>()->default_value("0"))
    ("c,chunks", "Number of chunks (default: uses many thin slices to optimize memory usage)", cxxopts::value<size_t>()->default_value("0"))
    ("p,threads", "Number of worker threads in thread pool (default: use one thread per logical CPU core)", cxxopts::value<size_t>()->default_value("0"));    

  if (argc == 1) {
    std::cout << opts.help() << std::endl;
    exit(-1);
  }

  try
  {
    opts.parse(argc, argv);
  }
  catch (std::exception &ex) {
    std::cout << "Error parsing options: " << ex.what() << std::endl;
    std::cout << opts.help() << std::endl;
    exit(-1);
  }  

  if (opts.count("h")) {
    std::cout << "CONFIGURED FOR INPUTS IN DIMENSION: " << embedding_dim << std::endl;
    std::cout << opts.help() << std::endl;
    exit(0);
  }

  std::string input_type_name = opts["t"].as<std::string>();
  if (input_type_name == "") {
    input_type_name = try_get_input_type_from_filename(opts["f"].as<std::string>());
  }

  if (input_type_name == "")
    input_type_name = "u8"; // this is the most typical type    

  if (input_type_name == "u8") {
    compute_euler_and_save_to_file<uint8_t>(opts);
  }
  else if (input_type_name == "s8") {
    compute_euler_and_save_to_file<int8_t>(opts);
  }
  else if (input_type_name == "u16") {
    compute_euler_and_save_to_file<uint16_t>(opts);
  }
  else if (input_type_name == "s16") {
    compute_euler_and_save_to_file<int16_t>(opts);
  }
  else if (input_type_name == "u32") {
    compute_euler_and_save_to_file<uint32_t>(opts);
  }
  else if (input_type_name == "s32") {
    compute_euler_and_save_to_file<int32_t>(opts);
  }
  else {
    std::cerr << "Wrong input encoding specification (-t option) (#bits per voxel and sign). Availble options: u8, s8, u16, s16, u32, s32, where 's' stands for signed integer, 'u' for unsigned and 8,16,32 is the number of bits. Exiting...";
  }

  return 0;
}
