/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once
#include "common.h"

/*
This file contains functions for streaming input of multidimensional images.
An image is read chunk by chunk. The actual voxel data is surrounded 
by padding to avoid bound-checking later.
The chunks have an overlap, so that it's possible to determine
which voxels introduce which cells. Care is to be taken so that
each cell (and its boundaries) are emitted once.

In 2D it looks like this:

Chunk with padding, x == max possible value
xxxxxxxxx
xpppppppx previous slice (or x)
xcccccccx current
xcccccccx
  ....
xcccccccx  current
xnnnnnnnx  next slice (or x)
xxxxxxxxx
*/


// Gets the size of a steam. The stream is modified but then reverted to
// the original position.
template <typename stream_t>
size_t get_file_size(stream_t &f) {
  const size_t old_pos = f.tellg();
  f.seekg(f.end);
  f.seekg(0, f.end);
  auto size = f.tellg();
  f.seekg(old_pos, f.beg);
  return size;
}

// Attempts the get a header of a file (if one is there) 
// encoding the size of a file. While it's preferred to 
// use other methods to supply the file name, 
// some of the existing voxel image files do use header.
// The header is assumed to be encoded the first D
// elements of the stream, where D is the dimension.
template <typename input_elem_t, typename stream_t>
index_t try_get_file_header(stream_t &f) {
  auto nr_elems = get_file_size(f) / sizeof(input_elem_t);
  const size_t old_pos = f.tellg();

  f.seekg(0, f.beg);

  index_t ind;
  for (int i = 0; i < embedding_dim; i++)
  {
    input_elem_t v;
    f.read(reinterpret_cast<char*>(&v),
      sizeof(input_elem_t));
    ind[i] = v;
  }

  f.seekg(old_pos, f.beg);

  if (nr_elems - embedding_dim != num_voxels(ind))
  {
    return index_t{ 0 }; // failed
  }

  return ind;
}

// Gets the number of BYTES needed to encode
// a header of a file.
template <typename input_elem_t, typename stream_t>
size_t get_header_size_in_bytes(stream_t &f)
{
  index_t ind = try_get_file_header<input_elem_t>(f);
  if (ind == index_t{ 0 })
    return 0;
  return embedding_dim * sizeof(input_elem_t);
}

// Reads a chunk of an image from disk.
template<typename input_elem_t>
matrix_t from_stream(std::string filename, index_t total_size, index_t chunk_size,
  size_t which_chunk, size_t num_chunks) {

  // std::cout << "READING CHUNK" << which_chunk << std::endl;

  // using input_value_t = file_elem_type;
  using input_value_t = input_elem_t;
  bool last = which_chunk + 1 == num_chunks;
  index_t slice_size = chunk_size;
  slice_size[0] = 1;
  std::ifstream stream(filename, std::ios::binary);

  size_t voxels = get_file_size(stream) / sizeof(input_elem_t);
  size_t header_size = get_header_size_in_bytes<input_elem_t>(stream);
  if (DEBUG) std::cout << "guessed header size: " << header_size << std::endl;

  const size_t voxels_per_slice = num_voxels(slice_size); // slice as in thin slice
  const size_t voxels_per_chunk = num_voxels(chunk_size);

  size_t offset_in_file = header_size + which_chunk * voxels_per_chunk * sizeof(input_value_t);
  if (which_chunk > 0)
    offset_in_file -= voxels_per_slice * sizeof(input_value_t);

  const size_t original_chunk_height = chunk_size[0];

  stream.seekg(offset_in_file); // move to beginning of our data   

  chunk_size += 2; // in each dimension
  chunk_size[0] += 2;
  matrix_t M(chunk_size);

  auto mn = std::numeric_limits<matrix_t::T_type>::min();
  auto mx = std::numeric_limits<matrix_t::T_type>::max();
  M.initialize(mx);

  blitz::RectDomain<embedding_dim> data_range(M.lbound() + 1, M.ubound() - 1);

  if (which_chunk == 0) data_range.lbound()[0] += 1;

  if (last)
  {
    data_range.ubound()[0] -= 1;
    int adjusted_data_height = total_size[0] - which_chunk * original_chunk_height;
    assert(original_chunk_height - adjusted_data_height >= 0);
    data_range.ubound()[0] -= original_chunk_height - adjusted_data_height;
    assert(adjusted_data_height + which_chunk * original_chunk_height == total_size[0]);
  }

  matrix_t submat = M(data_range);
  size_t elems = submat.numElements();

  for (auto it = submat.begin(), end = submat.end(); it != end; ++it) {
    input_value_t v;
    stream.read(reinterpret_cast<char*>(&v),
      sizeof(input_value_t));  // Slow, but not a bottleneck.    
    *it = v;
  }

  // function_value_t data_mn = *std::min_element(submat.begin(), submat.end());
  // function_value_t data_mx = *std::max_element(submat.begin(), submat.end());
  // std::cout << "mn " << data_mn << " mx " << data_mx << std::endl;

  return M;
}
