/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once

#include "common.h"

/*
  This file contains classes used to handle cubical cells and cubical complexes:
  - cube_handle is a lightweight representation of a cubical cell
  - cubical_complex is a heavy-weight representation of a cubical complex (usually chunk of an image)
    which requires heavy pre-computation during init.
*/

class cubical_complex;
class star_manager;


// Stores the essential information about a single cubical cell. 
// Can be used to access information about cell's faces etc by providing a 
// callback/visitor.
struct cube_handle {
  cube_id_t id;
  index_t big_ind;  // we don't always need that...
  function_value_t val;
  int type;  // can get from big_ind
  int dim;
  // int nr_in_voxel;

  /*cube_handle(cube_id_t id, const index_t &big_ind, function_value_t val = -1) :
    id(id), big_ind(big_ind), val(val) {
  }*/

  cubical_complex *comp;

  bool operator<(const cube_handle &other) const {
    return id < other.id;
  }

  //cube_handle(const cube_handle &) = delete;
  //cube_handle &operator=(const cube_handle &) = delete;

  template <typename fun_t>
  void for_each_face_in_star(fun_t f) const;

  template <typename fun_t>
  void for_each_face(fun_t f) const;

  template <typename fun_t>
  void for_each_boundary(fun_t f) const;
};

// Represents the state of a cubical cell complex.
// Requires heavy pre-computation at init.

class cubical_complex : public voxel_traits {
protected:
  matrix_t &m;
  void init_faces();
  void init_bounaries();

  // same indexing
  std::vector<index_t> face_index_deltas;
  std::vector<ptrdiff_t>
    neighbour_address_offsest;  // address offsets to neighbours
  std::vector<cube_id_t> face_id_deltas;

  std::unordered_map<cube_id_t, face_number_t> delta_to_face_number;

  std::vector<std::vector<index_t>> type_to_voxel_index_offset;
  std::vector<std::vector<ptrdiff_t>> type_to_voxel_address_offset;

  std::vector<int> face_dims;
  std::vector<int> face_types;

  std::vector<mask_t> face_masks;
  std::vector<mask_t> coface_masks;

  std::vector<std::vector<index_t>> boundary_deltas;
  std::vector<std::vector<int>> boundary_id_deltas;

  const index_t large_extent;

  bool is_inside_and_not_in_collar(const index_t &pos) {
    if (pos[0] <= 1 || pos[0] + 2 >= m.extent(0)) {
      return false;
    }

    for (size_t i = 1, n = pos.length(); i < n; ++i) {
      if (pos[i] <= 0 || pos[i] + 1 >= m.extent(i)) {
        return false;
      }
    }
    return true;
  }

public:
  cubical_complex(matrix_t &image)
    : m(image), large_extent(enlarge_extent(image.extent())) {
    init_faces();
    init_bounaries();
  }

  index_t get_voxel_small_index(const cube_id_t voxel_id) const {
    return ::get_index_with_extent(m.extent(), voxel_id);
  }

  cube_id_t get_voxel_small_id(const index_t big_ind) const {
    return ::get_id_with_extent(m.extent(), to_small_coords(big_ind));
  }

  cube_id_t get_voxel_small_id(cube_id_t big_id) const {
    return ::get_id_with_extent(m.extent(), to_small_coords(get_index_from_id(big_id)));
  }

  cube_id_t get_id(const cube_handle &cube) const {
    return ::get_id_with_extent(large_extent, cube.big_ind);
  }

  cube_id_t get_id(const index_t &cube_big_ind) const {
    return ::get_id_with_extent(large_extent, cube_big_ind);
  }

  index_t get_index_from_id(const cube_id_t id) const {
    return ::get_index_with_extent(large_extent, id);
  }

  size_t num_cells() const { return get_id(large_extent - 1) + 1; }

  size_t num_voxels() const { return m.numElements(); }

  const function_value_t get_value_from_voxel_id(cube_id_t id) const {
    return m(get_voxel_small_index(id));
  }

  const function_value_t get_value_from_cube_id(cube_id_t id) const {
    const auto big_index = get_index_from_id(id);
    const index_t voxel_index = to_small_coords(big_index); // from traits
    return m(voxel_index);
  }

  function_value_t& get_value_from_cube_id(cube_id_t id) {
    const auto big_index = get_index_from_id(id);
    const index_t voxel_index = to_small_coords(big_index); // from traits
    return m(voxel_index);
  }

  template <typename fun_t>
  void for_each_face_in_star_of(const cube_handle &c, fun_t f) {
    auto signature = get_star_signature(c);
    // this call could be slow for higher dim with bitset...
    this->for_each_face_of(c, f, ~signature);
  }

  template <typename fun_t>
  void for_each_proper_face_of(const cube_handle &c, fun_t f,
    mask_t excluding = mask_t(0)) {

    const mask_t one = 1;
    cube_handle face_cube{};
    face_cube.comp = this;
    face_cube.dim = c.dim;

    for (size_t i = 0, n = face_id_deltas.size(); i < n; i++) {
      if ((excluding & (one << i)) ==
        (one << i))  // VERY SLOW sloow for bitset!
        continue;

      const cube_id_t face_id = c.id + face_id_deltas[i];
      const int type_id = face_types[i];
      const int dim = face_dims[i];

      face_cube.type = type_id;
      face_cube.id = face_id;
      face_cube.dim = dim;
      // face_cube.nr_in_voxel = i;

      f(face_cube);
    }
  }

  template <typename fun_t>
  void for_each_face_of(const cube_handle &c, fun_t f,
    mask_t excluding = mask_t(0)) {
    const mask_t one = 1;
    cube_handle face_cube{};
    face_cube.comp = this;
    face_cube.dim = c.dim; // ?

    f(c); // run on itself?

    for (size_t i = 0, n = face_id_deltas.size(); i < n; i++) {
      if ((excluding & (one << i)) ==
        (one << i))  // VERY SLOW sloow for bitset!
        continue;

      const cube_id_t face_id = c.id + face_id_deltas[i];
      const int type_id = face_types[i];
      const int dim = face_dims[i];

      face_cube.type = type_id;
      face_cube.id = face_id;
      face_cube.dim = dim;
      // face_cube.nr_in_voxel = i;

      f(face_cube);
    }
  }

  template <typename fun_t>
  void for_each_voxel(fun_t f) {
    for (auto it = m.begin(), end = m.end(); it != end; ++it) {
      if (*it >= INFINITE_FUNCTION_VALUE)
        continue;

      const index_t ind = it.position();

      if (!is_inside_and_not_in_collar(ind)) {
        continue;
      }

      const index_t big = to_big_coords(ind);
      const cube_id_t id = get_id(big);
      const function_value_t val = *it;

      cube_handle c{ id, big, val };
      c.comp = this;
      c.dim = embedding_dim;
      c.type = VOXEL_TYPE_ID;
      f(c);
    }
  }

  template <typename fun_t>
  void for_each_interface_cube(fun_t f) {
    if (DEBUG)
      std::cout << "ALLOC" << 2 * num_cells() / 8 / 1'000'000. << " Mbytes in for_each_interface_cube" << std::endl;
    std::vector<bool> lower(num_cells());
    std::vector<bool> upper(num_cells());

    for (auto it = m.begin(), end = m.end(); it != end; ++it) {
      if (*it >= INFINITE_FUNCTION_VALUE)
        continue;

      const index_t ind = it.position();
      if (ind[0] != m.ubound(0) - 1 && ind[0] != m.ubound(0) - 2) // for two slices it's ok
        continue;

      bool in = true;
      for (size_t i = 1, n = ind.length(); i < n; ++i) {
        if (ind[i] <= 0 || ind[i] + 1 >= m.extent(i)) {
          in = false;
          break;
        }
      }

      if (!in) continue;

      const index_t big = to_big_coords(ind);
      const cube_id_t id = get_id(big);
      const function_value_t val = *it;

      cube_handle c{ id, big, val };
      c.comp = this;
      c.dim = embedding_dim;
      c.type = VOXEL_TYPE_ID;

      if (ind[0] == m.ubound(0) - 1) {
        c.for_each_face([&](const cube_handle &f) {
          lower[f.id] = true;
        });
      }
      else {
        c.for_each_face([&](const cube_handle &f) {
          upper[f.id] = true;
        });

      }
    }

    this->for_each_voxel_including_interface([&](const auto &v) {
      if (v.val < INFINITE_FUNCTION_VALUE)
        v.for_each_face_in_star([&](const auto &fc) {
        if (!lower[fc.id] || !upper[fc.id])
          return;
        f(fc);
      });
    });
  }

  template <typename fun_t>
  void for_each_upper_interface_cube(fun_t f) {
    if (DEBUG)
      std::cout << "ALLOC" << 2 * num_cells() / 8 / 1'000'000. << " Mbytes in for_each_upper_interface_cube" << std::endl;

    std::vector<bool> lower(num_cells());
    std::vector<bool> upper(num_cells());

    for (auto it = m.begin(), end = m.end(); it != end; ++it) {
      if (*it >= INFINITE_FUNCTION_VALUE)
        continue;

      const index_t ind = it.position();
      if (ind[0] != 1 && ind[0] != 2) // for two slices it's ok
        continue;

      bool in = true;
      for (size_t i = 1, n = ind.length(); i < n; ++i) {
        if (ind[i] <= 0 || ind[i] + 1 >= m.extent(i)) {
          in = false;
          break;
        }
      }

      if (!in) continue;

      const index_t big = to_big_coords(ind);
      const cube_id_t id = get_id(big);
      const function_value_t val = *it;

      cube_handle c{ id, big, val };
      c.comp = this;
      c.dim = embedding_dim;
      c.type = VOXEL_TYPE_ID;

      if (ind[0] == 1) {
        c.for_each_face([&](const cube_handle &f) {
          lower[f.id] = true;
        });
      }
      else {
        c.for_each_face([&](const cube_handle &f) {
          upper[f.id] = true;
        });
      }
    }

    this->for_each_voxel_including_interface([&](const auto &v) {
      if (v.val < INFINITE_FUNCTION_VALUE)
        v.for_each_face_in_star([&](const auto &fc) {
        if (!lower[fc.id] || !upper[fc.id])
          return;
        f(fc);
      });
    });
  }

  template <typename fun_t>
  void for_each_external_voxel(fun_t f) {
    for (auto it = m.begin(), end = m.end(); it != end; ++it) {
      if (*it >= INFINITE_FUNCTION_VALUE)
        continue;

      const index_t ind = it.position();

      if (ind[0] != m.ubound(0) - 1 && ind[0] != 1) // for two slices it's ok
        continue;

      bool in = true;
      for (size_t i = 1, n = ind.length(); i < n; ++i) {
        if (ind[i] <= 0 || ind[i] + 1 >= m.extent(i)) {
          in = false;
          break;
        }
      }

      if (!in) continue;

      const index_t big = to_big_coords(ind);
      const cube_id_t id = get_id(big);
      const function_value_t val = *it;

      cube_handle c{ id, big, val };
      c.comp = this;
      c.dim = embedding_dim;
      c.type = VOXEL_TYPE_ID;

      f(c);
    }
  }

  template <typename fun_t>
  void for_each_upper_interface_cube_SLOW(fun_t f) {
    std::set<cube_handle> lower;
    std::set<cube_handle> upper;

    for (auto it = m.begin(), end = m.end(); it != end; ++it) {
      const index_t ind = it.position();

      if (ind[0] != 1 && ind[0] != 2) // for two slices it's ok
        continue;

      bool in = true;
      for (size_t i = 1, n = ind.length(); i < n; ++i) {
        if (ind[i] <= 0 || ind[i] + 1 >= m.extent(i)) {
          in = false;
          break;
        }
      }

      if (!in) continue;

      const index_t big = to_big_coords(ind);
      const cube_id_t id = get_id(big);
      const function_value_t val = *it;

      if (val >= INFINITE_FUNCTION_VALUE)
        continue;

      cube_handle c{ id, big, val };
      c.comp = this;
      c.dim = embedding_dim;
      c.type = VOXEL_TYPE_ID;

      if (ind[0] == 1) {
        c.for_each_face([&](const cube_handle &f) {
          lower.insert(f);
        });
      }
      else {
        c.for_each_face([&](const cube_handle &f) {
          upper.insert(f);
        });

      }
    }
    std::set<cube_handle> inter;
    std::set_intersection(lower.begin(), lower.end(), upper.begin(), upper.end(), std::inserter(inter, inter.begin()));
    for (const auto &c : inter)
    {
      f(c);
    }
  }

  template <typename fun_t>
  void for_each_voxel_including_interface(fun_t f) {
    for (auto it = m.begin(), end = m.end(); it != end; ++it) {
      if (*it >= INFINITE_FUNCTION_VALUE)
        continue;

      const index_t ind = it.position();

      const index_t big = to_big_coords(ind);
      const cube_id_t id = get_id(big);
      const function_value_t val = *it;

      cube_handle c{ id, big, val };
      c.comp = this;
      c.dim = embedding_dim;
      c.type = VOXEL_TYPE_ID;
      f(c);
    }
  }

  //
  const std::vector<index_t> &get_containing_voxels_offsets(
    const index_t &ind) const {
    const int type = get_cube_type(ind);
    return type_to_voxel_index_offset[type];
  }

  index_t get_voxel_with_star_containing_face(const cube_id_t id) {
    const index_t ind = get_index_from_id(id);
    const auto &big_indices_of_voxels = get_containing_voxels_offsets(ind);
    const auto mx_val = std::numeric_limits<function_value_t>::max();
    std::tuple<function_value_t, const function_value_t *, index_t> best(
      mx_val, 0, index_t{});
    for (const auto &off : big_indices_of_voxels) {
      const index_t global = off + ind;
      const index_t image_index = to_small_coords(global);
      const auto &val = m(image_index);

      best = std::min(best, std::make_tuple(val, &val, global));
    }
    return std::get<2>(best);
  }

  void enforce_subcomplex_property(mask_t &m) const {
    // this could be faster, we only need to access set bits...
    for (size_t i = 0; i < neighbour_address_offsest.size(); i++) {
      if ((m & (mask_t(1) << i)) == (mask_t(1) << i))  // sloow for d > 3
      {
        const mask_t &sub_mask = face_masks[i];
        if ((m | sub_mask) != m)  // not all faces are present
          m ^= (mask_t(1) << i);  // remove the current cell, because it has missing (co)faces
      }
    }
  };

  void dump_star(const mask_t &m);

  void dump_cubemap(std::vector<bool> &is_critical, int offset, int slice);

  template <typename fun_t>
  void for_each_boundary_of(const cube_handle &c, fun_t f) {
    cube_handle bdry_cube = {};
    bdry_cube.dim = c.dim - 1;
    bdry_cube.comp = this;

    for (const int delta : boundary_id_deltas[c.type]) {
      const cube_id_t boundary_elem_id = c.id + delta;
      bdry_cube.id = boundary_elem_id;
      // bdry_cube.type = get_cube_type(get_index_from_id(bdry_cube.id));
      f(bdry_cube);
    }
  }

  template <typename a_t, typename b_t>
  bool is_my_face_with_tiebreak(const a_t &my_value, const b_t &his_value) {
    return is_this_my_face(my_value, his_value) ||
      (my_value == his_value && &my_value < &his_value);
  }

  mask_t get_star_signature(const cube_handle &c) {
    return get_star_signature(c.big_ind);
  }

  mask_t get_star_signature(const index_t &big_ind) {
    mask_t signature = 0;
    const mask_t one = 1;

    const index_t source_index = to_small_coords(big_ind);
    const function_value_t* ptr = &m(source_index);
    const auto &my_val = *ptr;

    mask_t excluded = 0;
    for (size_t i = 0; i < face_index_deltas.size(); ++i) {
      if ((excluded & (one << i)) == (one << i))  // For general bitsets
        continue;

      const auto off = neighbour_address_offsest[i];
      const function_value_t &his_val = *(ptr + off);

      if (is_my_face_with_tiebreak(my_val, his_val)) {
        signature |= (one << i);
      }
      else {
        excluded |= coface_masks[i];  // prevent checking of cofaces of i
      }
    }

    return signature;
  }

  std::vector<index_t> possible_voxels_SLOW(const index_t source,
    const index_t face_ind) {
    std::vector<index_t> result;

    const int degeneracy = 1 - source(0) % 2;
    int num_degeneracies = 0;
    for (auto coord : face_ind) num_degeneracies += ((coord % 2) == degeneracy);

    for (size_t m = 0; m < (1ULL << num_degeneracies); m++) {
      int curr_degeneracy = 0;
      index_t ret(0);
      for (size_t i = 0; i < face_ind.length(); i++) {
        if (face_ind[i] % 2 == degeneracy) {
          ret[i] = (m & (1ULL << curr_degeneracy)) ? 1 : -1;
          curr_degeneracy++;
        }
      }
      result.emplace_back(ret);
    }
    return result;
  }
};

void cubical_complex::init_faces() {
  face_index_deltas = get_voxel_face_deltas();
  face_index_deltas.erase(face_index_deltas.begin());
  assert(0 == std::count(face_index_deltas.begin(), face_index_deltas.end(),
    index_t(0)));

  index_t origin = get_base();  // we use origin+delta to make sure everything
                                // is nonnegative.
  if (origin(0) == 0) origin += 2;

  std::sort(
    face_index_deltas.begin(), face_index_deltas.end(),
    [&](const auto &a, const auto &b) { return get_dim(a) < get_dim(b); });

  for (auto delta : face_index_deltas) {
    const int id_delta =
      static_cast<int>(get_id(origin + delta)) - get_id(origin);
    const auto face_type = get_cube_type(index_t(origin + delta));
    const auto dim = get_dim(index_t(origin + delta));

    face_types.emplace_back(face_type);
    face_id_deltas.emplace_back(id_delta);
    face_dims.emplace_back(dim);
  }

  for (size_t i = 0; i < face_id_deltas.size(); ++i) {
    delta_to_face_number[face_id_deltas[i]] = static_cast<face_number_t>(i);
  }

  // Doesn't really matter just some voxel in the middle, so that
  // we don't get negative indices and
  // can generate all directions as shifts in linear memory.
  // Note: this is not big-to-small-index translation! 
  const index_t mid = m.extent() / 2;

  for (const index_t delta : face_index_deltas) {
    const index_t ind = mid + delta;
    ptrdiff_t off = &m(ind) - &m(mid);
    neighbour_address_offsest.emplace_back(off);
  }

  auto is_subface = [](index_t a, index_t b) -> bool {
    for (size_t i = 0; i < a.length(); i++)
      if (a[i] != 0 && a[i] != b[i]) return false;
    return true;
  };

  for (int i = 0; i < face_index_deltas.size(); i++) {
    mask_t mask = 0;
    for (int j = 0; j < face_index_deltas.size(); j++)
      if (is_subface(face_index_deltas[j], face_index_deltas[i]))
        mask |= (mask_t(1) << j);
    face_masks.push_back(mask);
  }

  for (int i = 0; i < face_index_deltas.size(); i++) {
    mask_t mask = 0;
    for (int j = 0; j < face_index_deltas.size(); j++)
      if (is_subface(face_index_deltas[i],
        face_index_deltas[j]))  // i,j swapped
        mask |= (mask_t(1) << j);
    coface_masks.push_back(mask);
  }
}

void cubical_complex::init_bounaries() {
  const size_t num_types = 1 << embedding_dim;
  boundary_deltas.resize(num_types);
  boundary_id_deltas.resize(num_types);
  type_to_voxel_index_offset.resize(num_types);
  type_to_voxel_address_offset.resize(num_types);

  for (size_t type_id = 0; type_id < (1ULL << embedding_dim); type_id++) {
    index_t ind(0);
    for (size_t i = 0; i < embedding_dim; i++) {
      if ((type_id & ((1ULL) << i))) {
        ind[embedding_dim - i - 1] = 1;
      }
    }

    const size_t computed_type = get_cube_type(ind);
    assert(computed_type == type_id);

    const index_t big_ind = ind;

    type_to_voxel_index_offset[type_id] =
      possible_voxels_SLOW(get_base(), big_ind);

    // Just some guy in the middle to avoid negative indices.
    // Note: this is not big-to-small-index translation! 
    const index_t mid = m.extent() / 2;
    for (const index_t delta : type_to_voxel_index_offset[type_id]) {
      const index_t ind = mid + delta;
      ptrdiff_t address_off = &m(ind) - &m(mid);
      type_to_voxel_address_offset[type_id].emplace_back(address_off);
    }

    for (size_t i = 0; i < embedding_dim; i++)
      if (ind[i] == 1) {
        index_t delta_1{ 0 };
        index_t delta_2{ 0 };
        delta_1[i] = 1;
        delta_2[i] = -1;

        boundary_deltas[type_id].emplace_back(delta_1);
        boundary_deltas[type_id].emplace_back(delta_2);

        int delta1 =
          static_cast<int>(get_id(big_ind + delta_1)) - get_id(big_ind);
        int delta2 =
          static_cast<int>(get_id(big_ind + delta_2)) - get_id(big_ind);

        boundary_id_deltas[type_id].emplace_back(delta1);
        boundary_id_deltas[type_id].emplace_back(delta2);
      }
  }
}

void cubical_complex::dump_cubemap(std::vector<bool> &is_critical, int offset, int slice) {
  if (embedding_dim != 2) {
    std::cout << "outputting cubemap only supported in 2D!" << std::endl;
    return;
  }

  if (is_critical.size() > 10000) {
    std::cout << "outputting cubemap only supported for small 2D images" << std::endl;
    return;
  }

  blitz::Array<std::string, embedding_dim> M(large_extent);

  std::string pat_by_type[] = { ".", "_", "|", "x" };

  std::stringstream ss;
  ss << "vis_slice_" << slice << ".py";
  std::ofstream pout(ss.str());

  assert(pout.good());

  pout << "from vislib import *" << std::endl;

  float shrink = 0.8f;  

  this->for_each_voxel_including_interface([&](const cube_handle &v) {
    if (v.val >= INFINITE_FUNCTION_VALUE) return; // continue		
    const mask_t sign = this->get_star_signature(v) | mask_t(1);

    auto center = get_index_from_id(v.id);

    v.for_each_face_in_star([&](const cube_handle &f)
    {
      const auto ind = get_index_from_id(f.id);
      const auto type = ::get_cube_type(ind);

      int gid = f.id + offset * slice;

      switch (type) {
      case(0) : {
        blitz::TinyVector<float, embedding_dim> pos = ind;

        pos = center + (pos - center)*shrink;
        pout << "draw_pt(" << pos[1] << "," << pos[0]
          << (is_critical[f.id] ? ",True" : ",False")
          << ", gid = " << gid
          << ")" << std::endl;
        break;
      }
      case(1) : case(2) : {
        std::vector<blitz::TinyVector<float, embedding_dim>> edge;

        f.for_each_boundary([&](const cube_handle &bd) {
          blitz::TinyVector<float, embedding_dim> pos = get_index_from_id(bd.id);
          pos = center + (pos - center)*shrink;
          edge.emplace_back(pos);
          //pout << "draw_edge(" << pos[1] << "," << pos[0] << ")" << std::endl;						
        });

        pout << "draw_edge(";
        for (int i = 0; i < 2; i++)
          pout << edge[i][1] << "," << edge[i][0] << (i ? "" : ",");

        pout << (is_critical[f.id] ? ",True" : ",False")
          << ", gid = " << gid
          << ");" << std::endl;
        break;
      }
      case(3) : {
        auto val = v.val;
        if (val < 0) val = -(val + 1000); // ...
        pout << "draw_voxel(" << center[1] << ", " << center[0] << "," << val
          << (is_critical[f.id] ? ",True" : ",False")
          << ", gid = " << gid
          << ");" << std::endl;
        break;
      }
      }
    });
  });

  pout << "plt.show()";
}

void cubical_complex::dump_star(const mask_t &m) {
  std::cout << "star config " << m << ": ";
  for (size_t i = 0; i < face_index_deltas.size(); i++) {
    if ((m & (mask_t(1) << i)) == (mask_t(1) << i))  // sloow for d > 3
    {
      std::cout << index_t(this->get_base() + face_index_deltas[i]) << " ";
    }
  }

  if (embedding_dim == 2) {
    bool x[3][3] = {};

    for (size_t i = 0; i < face_index_deltas.size(); i++) {
      if ((m & (mask_t(1) << i)) == (mask_t(1) << i))
      {
        index_t pos = this->get_base() + face_index_deltas[i];
        x[pos(0)][pos(1)] = true;
      }
    }

    std::cout << std::endl;

    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) std::cout << (x[i][j] ? "*" : ".");
      std::cout << std::endl;
    }
  }
  std::cout << std::endl;
}

template <typename fun_t>
void cube_handle::for_each_face_in_star(fun_t f) const {
  comp->for_each_face_in_star_of(*this, f);
}

template <typename fun_t>
void cube_handle::for_each_face(fun_t f) const {
  comp->for_each_face_of(*this, f);
}

template <typename fun_t>
void cube_handle::for_each_boundary(fun_t f) const {
  comp->for_each_boundary_of(*this, f);
}
