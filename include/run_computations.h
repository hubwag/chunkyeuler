/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once

#include <mutex>

#include "common.h"
#include "streaming.h"
#include "euler.h"
#include "image_statistics.h"
#include "misc.h"

// Saves the Euler characteristic curve to a file.
void save_euler_curve_to_file(const std::string &output_filename, const std::vector<euler_pair_t> &euler_char)
{  
  std::ofstream of(output_filename);
  for (auto p : euler_char)
    of << p.first << ' ' << p.second << std::endl;  

  std::cout << "EULER characteristic curve saved to: " << output_filename << std::endl;
}

// Computes the Euler characteristic curve based on the provided options.
template<typename input_elem_t>
std::vector<euler_pair_t> compute_euler(cxxopts::Options &opts)
{
  const size_t w = opts["w"].as<size_t>();
  const std::string filename = opts["f"].as<std::string>();
  const std::string sz_str = opts["s"].as<std::string>();  
  const index_t sz_from_options = w > 0 ? index_t(w) : parse_size(sz_str);
  size_t num_chunks = opts["c"].as<size_t>();
  size_t num_threads = opts["p"].as<size_t>();

  check_file_or_exit(filename);

  if (num_threads == 0)
    num_threads = std::thread::hardware_concurrency();  

  index_t input_sz = get_input_dimensions_or_exit<input_elem_t>(filename, sz_from_options);  

  size_t chunk_height = get_chunk_size_or_exit(input_sz, &num_chunks);

  index_t slice_sz = input_sz;
  slice_sz[0] = chunk_height;

  std::cout << "We will do: " << num_chunks << " slices, each of size " << slice_sz
    << " = " << num_voxels(slice_sz) << " voxels." << std::endl
    << std::endl;

  index_t sz_with_collar = slice_sz + 2;
  sz_with_collar[0] += 2;
  const size_t num_voxels_including_collar = num_voxels(sz_with_collar);  

  if (num_threads > std::thread::hardware_concurrency())
    std::cout << "You requested " << num_threads << " but your hardware supports " << std::thread::hardware_concurrency() << " hardware threads. " << std::endl;

  ctpl::thread_pool tp(num_threads);

  // This chooses the sparse or dense version depending on the input encoding.
  using euler_merger_t = merger_chooser<function_value_t>::type;
  euler_merger_t merger(num_chunks);
  image_statistics stats(num_chunks);
  
  int done_chunks = 0;
  const int report_each = int(sqrt(num_chunks));
  using mutex_lock_guard = std::lock_guard<std::mutex>;
  std::mutex progress_report_mutex;

  Stopwatch sw_without_preproc;

  for (size_t current_chunk = 0; current_chunk < num_chunks; current_chunk++) {
    tp.push(
      [&, current_chunk = current_chunk](int) -> void {
      Stopwatch st_data;
      matrix_t m = from_stream<input_elem_t>(filename, input_sz, slice_sz, current_chunk, num_chunks);

      update_image_statistics(m, &stats, current_chunk);
      compute_euler_of_slice_with_merger(m, merger, current_chunk);
      {
        mutex_lock_guard lock(progress_report_mutex);
        if (DEBUG) std::cerr << "File read in:" << st_data.lap() << std::endl;
        done_chunks++;

        if (done_chunks % report_each == 0) {
          std::cout << "\r";
          std::cout << "Done: " << 100.0*done_chunks / num_chunks << " % " << std::flush;
        }
      }
    });
  }

  tp.stop(true);

  std::cout << "\nMIN VALUE IN ALL IMAGE IS: " << stats.get_min_value() << std::endl;
  std::cout << "\nMAX VALUE IN ALL IMAGE IS: " << stats.get_max_value() << std::endl;
  auto euler_char = merger.get_merged_euler_characteristic();
  std::cerr << "\nEULER characteristic curve computation took "
    << sw_without_preproc.lap() << " seconds\n" << std::endl;

  return euler_char;
}

// Computes the Euler characteristic curve based on the provided options and
// saves it to a file.
template<typename input_elem_t>
void compute_euler_and_save_to_file(cxxopts::Options &opts)
{
  auto euler_char = compute_euler<input_elem_t>(opts);
  const std::string filename = opts["f"].as<std::string>();
  save_euler_curve_to_file(filename + ".euler", euler_char);
}
