/*  Copyright 2015-2018 IST Austria
    File contributed by: Teresa Heiss, Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once 

#include "common.h"
#include "cubical_complex.h"

/*
This file contains functions for computing the Euler characteristic curves for images
and additional utilities.
*/

/* Converts the changes/deltas in Euler characteristic into the
final euler characteristic curve. */
std::vector<euler_pair_t> from_map_of_changes_to_euler(
  const euler_map_t &euler_changes_map) {

  // Converting the unordered_map into a vector
  // and leaving out the pairs that indicate no change in the euler char.
  std::vector<euler_pair_t> euler;
  for (euler_pair_t function_value_and_euler : euler_changes_map) {
    if (function_value_and_euler.second != 0) {
      euler.push_back(function_value_and_euler);
    }
  }

  // Sorting (lexicographically)
  std::sort(euler.begin(), euler.end());

  // Accumulating over the vector of changes to get the euler characteristic
  for (size_t i = 1; i < euler.size(); i++) {
    euler[i].second += euler[i - 1].second;
  }

  // The second element of the last entry in euler should be 1,
  // because this is the euler characteristic of a completely filled rectangle.
  assert(euler.back().second == 1);

  return euler;
}

/* Accumulates the changes in the Euler characteristic curve
from multiple slices. This is a sparse version suitable for 
images with large value span (int32/int64 or float/double).
*/
class euler_merger_sparse {
protected:
  std::vector<euler_map_t> euler_changes;
  size_t num_chunks;

public:
  euler_merger_sparse(size_t number_of_chunks = 1)
    : num_chunks(number_of_chunks),
    euler_changes(number_of_chunks) {
  }

  void update(function_value_t function_value, int change, size_t slice_index) {
    assert(slice_index >= 0);
    assert(slice_index < euler_changes.size());
    euler_changes[slice_index][function_value] += change;
  }

  std::vector<euler_pair_t> get_merged_euler_characteristic() {
    //Summing num_chunks many unordered_maps up to one unordered_map
    euler_map_t euler_changes_overall;
    for (size_t slice = 0; slice < num_chunks; slice++) {
      for (auto function_value_and_change : euler_changes[slice]) {
        function_value_t function_value = function_value_and_change.first;
        auto change = function_value_and_change.second;
        euler_changes_overall[function_value] += change;
      }
    }

    std::vector<euler_pair_t> euler =
      from_map_of_changes_to_euler(euler_changes_overall);

    return euler;
  }
};

/* Accumulates the changes in the Euler characteristic curve
from multiple slices.This is a dense version suitable for
images with small value span (int8/int16).*/
template<typename input_elem_t>
class euler_merger {
protected:
  std::vector<std::vector<int>> euler_changes;
  size_t num_chunks;
  function_value_t min_val;
  function_value_t max_val;

public:
  euler_merger(size_t number_of_chunks = 1,
    function_value_t maximum_value =
      static_cast<function_value_t>(std::numeric_limits<input_elem_t>::max()),
    function_value_t minimum_value = 0)
    : num_chunks(number_of_chunks),
    max_val(maximum_value),
    min_val(minimum_value),
    euler_changes(number_of_chunks) {
    assert(minimum_value <= maximum_value);
    for (size_t slice = 0; slice < number_of_chunks; slice++) {
      euler_changes[slice].resize(maximum_value - minimum_value + 1);
    }
  }

  void update(function_value_t value, int change, size_t slice_index) {
    assert(slice_index >= 0);
    assert(slice_index < euler_changes.size());
    assert(value - min_val >= 0);
    size_t index = value - min_val;
    assert(index < euler_changes[slice_index].size());

    euler_changes[slice_index][index] += change;
  }

  std::vector<euler_pair_t> get_merged_euler_characteristic() {
    std::vector<int> euler_whole(max_val - min_val + 1, 0);
    std::vector<euler_pair_t> euler_sparse;

    //Summing num_chunks many vectors up to one vector
    for (auto eul_changes_of_slice : euler_changes) {
      assert(euler_whole.size() == eul_changes_of_slice.size());
      for (size_t j = 0; j < euler_whole.size(); j++) {
        euler_whole[j] += eul_changes_of_slice[j];
      }
    }

    //Making the vector 'sparse'
    // i.e.: Collecting all the entries where the euler characteristic changes
    if (euler_whole[0] != 0) {
      euler_sparse.push_back(euler_pair_t(min_val, euler_whole[0]));
    }
    for (size_t i = 1; i < euler_whole.size(); i++) {
      euler_whole[i] += euler_whole[i - 1];
      if (euler_whole[i] != euler_whole[i - 1]) {
        euler_sparse.push_back(
          euler_pair_t(min_val + static_cast<function_value_t>(i), euler_whole[i])
        );
      }
    }

    //The last entry in euler should be 1,
    // because this is the euler characteristic of a completely filled rectangle.
    assert(euler_whole.back() == 1);
    assert(euler_sparse.back().second == 1);

    return euler_sparse;
  }
};

/* Computes the Euler characteristic curve of a chunk of an image using
the provided (sparse/dense) merger class.
*/
template<typename euler_merger_t>
void compute_euler_of_slice_with_merger(matrix_t &m,
  euler_merger_t &eul_merger,  
  size_t current_slice = 0) {


  if (DEBUG)
  std::cout << "START computing Euler of slice " << current_slice
    << " with merger" << std::endl;
  Stopwatch sw;
  cubical_complex complex(m);

  complex.for_each_voxel([&](const cube_handle &x) {
    x.for_each_face_in_star([&](const cube_handle &c) {
      if (c.dim % 2 == 1) { //odd
        eul_merger.update(x.val, -1, current_slice);
      }
      else { //even
        eul_merger.update(x.val, +1, current_slice);
      }
    });
  });

  if (DEBUG)
  std::cout << "Computed Euler of slice " << current_slice
    << " with merger in " << sw.lap() << std::endl;
}

/* Computes an Euler characteristic curve from a persistence diagram.
Useful for checking the correctness of persistence computations.
*/
std::vector<euler_pair_t> from_persistence_to_euler_sparse(
  const persistence_diagram_t &persistence,
  function_value_t real_minimum_value) {

  euler_map_t euler_map;

  //The one connected component that never dies increases the euler char.
  euler_map[real_minimum_value]++;

  for (auto const &triple : persistence) {
    int dim = std::get<0>(triple);
    function_value_t birth = std::get<1>(triple);
    function_value_t death = std::get<2>(triple);

    if (dim % 2 == 1) { //odd
      euler_map[birth]--;
      euler_map[death]++;
    }
    else { //even
      euler_map[birth]++;
      euler_map[death]--;
    }
  }

  std::vector<euler_pair_t> euler = from_map_of_changes_to_euler(euler_map);
  return euler;
}

// Chooses the version of the Euler merger based on the image value type.
// By default chooses the sparse version, template specialization below override
// this behavior.
template<typename input_type_t>
struct merger_chooser
{
  using type = euler_merger_sparse;
};

// Chooses the version of the Euler merger based on the image value type:
// uint_8 yields the dense version.
template<>
struct merger_chooser<uint8_t>
{
  using type = euler_merger<uint8_t>;
};

// Chooses the version of the Euler merger based on the image value type:
// uint_16 yields the dense version.
template<>
struct merger_chooser<uint16_t>
{
  using type = euler_merger<uint16_t>;
};
