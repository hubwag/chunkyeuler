/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once

/*
  This file contains definitions needed to compare and output BLITZ indices (short vectors).
*/

namespace blitz {

bool operator<(const index_t &a, const index_t &b) {
  return std::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end());
}

bool operator==(const index_t &a, const index_t &b) {
  return std::equal(a.begin(), a.end(), b.begin(), b.end());
}

template <typename T1, typename T2>
std::ostream &operator<<(std::ostream &os, const std::pair<T1, T2> &x) {
  return os << x.first << "," << x.second;
}

}
