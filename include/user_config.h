/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once

const bool DEBUG = false;

#ifndef embedding_dim
#define embedding_dim 3u
#endif

// const size_t embedding_dim = 3;
using cube_id_t = int64_t;

// Below are parameters for fine tuning. Defaults should work for typical inputs.

using function_value_t = int32_t;
using matrix_t = blitz::Array<function_value_t, embedding_dim>;
using index_t = blitz::TinyVector<int32_t, embedding_dim>;

const function_value_t INFINITE_FUNCTION_VALUE =
std::numeric_limits<function_value_t>::max();

using euler_pair_t = std::pair<function_value_t, int64_t>;
using euler_map_t = std::unordered_map<function_value_t, int64_t>;

using persistence_diagram_t =
std::vector<std::tuple<int, function_value_t, function_value_t>>;
