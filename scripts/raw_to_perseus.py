"""  Copyright 2015-2018 IST Austria
    File contributed by: Teresa Heiss, Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. """

import struct
import sys

#example = 'c:/work/hydrogenAtom_128x128x128.raw'

def raw_to_perseus(filename, sz):
    sz.reverse()
    dim = len(sz)
    num_voxels = reduce(lambda a,b: a*b, map(int, sz), 1)
    values = open(filename, "rb").read()

    assert len(values) == num_voxels, "Incorrect image size"

    with open(filename + '.cub.perseus', 'wt') as out:
        out.write(('{}\n' + '{}\n'*dim).format(dim, *sz))
        
        for v in values:
            x = struct.unpack('B', v)[0]
            out.write(str(x) + '\n')
    
    print('output succesfully written to: {}'.format(filename + '.cub.perseus'))

if __name__ == '__main__':
    if len(sys.argv) <= 3:
        print('Converts an arbitrary dimensional image file from u8 (unsigned 8-bit integer) encoded raw format to perseus ascii format.')
        print('Output is written with suffix .cub.perseus')
        print('usage: python {} <filename.raw> <size0> <size1> <size2> ...'.format(sys.argv[0]))
        print('Exitting...')
        exit(-1)
        
    raw_to_perseus(sys.argv[1], sys.argv[2:])
