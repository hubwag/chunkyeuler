/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. */

#include "cubical_complex.h"
#include "common.h"

/*
  This file contains several helper functions for 
  parsing input options and reporting progress.
*/

// Parses the filename to get a file size. Returns a zero size on failure.
// The filename is assumed to contain a substring: W1xW2x...Wdim encoding the size.
index_t try_get_size_from_filename(std::string s) {
  // Note the non-greedy match to avoid eating up the first number
  std::string pattern = ".*?(\\d+)";
  for (size_t i = 0; i + 1 < embedding_dim; i++)
    pattern += "[xX](\\d+)";
  pattern += ".*";

  std::regex re(pattern);

  std::smatch m;
  if (std::regex_search(s, m, re))
  {
    // m contains also the entire match, that we don't need.
    if (m.size() != embedding_dim + 1)
      return index_t{ 0 };

    index_t sz;
    for (int i = 0; i < embedding_dim; i++)
    {
      // m[0] is the whole match, we skip it.
      sz[i] = std::stoi(m[i + 1].str());
    }
    return sz;
  }
  return index_t{ 0 };
}

// Parses the filename to get a file size. Returns a zero size on failure.
// The filename is assumed to contain a substring: W1xW2x...Wdim encoding the size.
std::string try_get_input_type_from_filename(std::string s) {
  // Note the non-greedy match to avoid eating up the first number
  std::string pattern = ".*?_t_([us](?:8|16|32))_.*";

  std::regex re(pattern);

  std::smatch m;
  if (std::regex_search(s, m, re) && m.size() > 1)
  {
    return m[1];
  }
  return "";
}

// Parses the input size string and returns it as an index.
index_t parse_size(std::string s)
{
  std::replace(s.begin(), s.end(), 'x', ' ');
  std::stringstream ss(s);
  index_t ind{ 0 };
  for (int i = 0; i < ind.length() && ss.good(); i++)
    ss >> ind[i];
  return ind;
}

void check_file_or_exit(std::string filename) {
  std::ifstream f(filename, std::ios::binary);
  if (!f.good()) {
    std::cerr << "could not open file " << filename << std::endl;
    exit(-1);
  }
}

/*
  Tries to determine input dimensions of a given file, given an initial guess.  
  1) Tries to use the initial guess, if provided.
  2) Tries to parse a header at the beginning of a file (see below).
  3) Tries to parse the filename for input size.

  If the determined size is too small (compared to the real size), it exists. If it's too large, 
  we assume it's fine and the user uses only part of the input 
  (for example to gauge the speed).
*/
template<typename file_elem_t>
index_t get_input_dimensions_or_exit(const std::string filename, index_t sz)
{
  std::ifstream f(filename, std::ios::binary);

  size_t file_size = get_file_size(f);
  std::cerr << "file of size: " << get_file_size(f) << " Bytes" << std::endl;

  const size_t num_elems_in_file = file_size / sizeof(file_elem_t);

  std::cout << "found " << num_elems_in_file << " voxels, each encoded on " << sizeof(file_elem_t) << " bytes" << std::endl;

  if (sz == index_t{ 0 })
  {
    sz = try_get_file_header<file_elem_t>(f);
    if (sz == index_t{ 0 }) {
      std::cout << "File Header not found or inconsistent with file length. Will need to determine dimensions differently." << std::endl;
    }
  }

  if (sz == index_t{ 0 })
    sz = try_get_size_from_filename(filename);

  if (sz == index_t{ 0 }) // still unspecified
  {
    std::cout << "dimensions of file were not specified (using -w or -s) and could not be read from file header. Quitting...";
    exit(-1);
  }

  std::cout << "\n\nusing size: " << sz << "\n" << std::endl;

  const size_t num_input_voxels = num_voxels(sz);
  const index_t large_sz = sz;

  if (static_cast<cube_id_t>((1u << embedding_dim) * num_elems_in_file) >
    std::numeric_limits<cube_id_t>::max()) {
    std::cerr
      << "I think your **cube_id_t** type is too small to index all the "
      << file_size / sizeof(file_elem_t) * (1 << embedding_dim)
      << " cells... exiting";
    exit(-1);
  }

  if (num_elems_in_file < num_input_voxels) {
    std::cerr << "I think your input is too small compared to the declared size... exiting";
    exit(-1);
  }

  if (num_elems_in_file > num_input_voxels) {
    std::cerr << "\n\tI think your file is too *large* compared to the declared size but I'll assume"
      " you know what you are doing (e.g. trying to compute a portion of a file)." << " expected: "
      << num_input_voxels << " but file contains: " << num_elems_in_file << std::endl << std::endl;
  }

  return sz;
}

// Tries to get the size of chunks for a given input and number of chunks. 
size_t get_chunk_size_or_exit(index_t input_sz, size_t *num_chunks)
{
  const int preferred_slice_size = 4;
  *num_chunks = std::min<size_t>(input_sz[0], *num_chunks);
  if (*num_chunks == 0)
    *num_chunks = std::max(1, input_sz[0] / preferred_slice_size);
  size_t chunk_height = input_sz[0] / *num_chunks;
  *num_chunks += (input_sz[0] % *num_chunks + chunk_height - 1) / chunk_height;

  assert(*num_chunks * chunk_height >= input_sz[0]);
  assert((*num_chunks - 1) * chunk_height < input_sz[0]);
  assert(*num_chunks <= input_sz[0]);

  if (input_sz[0] <= 2 && *num_chunks > 1)
  {
    std::cout << "For technical reasons it's not possible to use multiple slices/chunks for files with the first dimension <= 2. Just use a single chunk (-c 1). Exitting.";
    exit(-1);
  }

  return chunk_height;
}

void report_matrix_size(const size_t unprocessed_matrix_weight)
{
  std::cerr << "the full sparse boundary matrix would have "
    << unprocessed_matrix_weight / 1'000'000'000.
    << " Bill ONES and would take: "
    << unprocessed_matrix_weight * 8 / 1'000'000'000.
    << " GB in binary format" << std::endl;
}

void report_matrix_size_comparison(size_t tot, const size_t unprocessed_matrix_weight)
{
  std::cout << "\n\tTOTAL WEIGHT: " << tot / 1'000'000.
    << "M vs normal: " << unprocessed_matrix_weight / 1'000'000. << "M elems"
    << " RATIO (higher is better): "
    << unprocessed_matrix_weight*1.0 / tot << std::endl << std::endl;
}

void verify_using_euler(size_t num_chunks,
  const persistence_diagram_t &p,
  std::vector<euler_pair_t> euler_char,
  function_value_t real_minimum_value)
{
  auto pers_euler = from_persistence_to_euler_sparse(p, real_minimum_value);
  if (pers_euler != euler_char)
    std::cout << "WRONG EULER" << " expected:\n" << euler_char << " got:\n" << pers_euler << std::endl;
  else std::cout << "SUCCESSFULLY VERIFIED THE COMPUTATIONS USING EULER CHARACTERISTIC!" << std::endl;

  assert(pers_euler == euler_char);
}

void report_remaining_time_estimate(size_t num_chunks, int s, size_t time_so_far)
{
  auto remaining_chunks = num_chunks - s - 1;
  float avg_time = time_so_far*1.0f / (s + 1);
  float remaining_time = remaining_chunks * avg_time;

  std::cout << "\tprojected remaining time: " << remaining_time << " s = " << remaining_time / 60.0f << " min" << std::endl << std::endl;
}

void update_image_statistics(matrix_t m, image_statistics *stats, size_t chunk) {
  cubical_complex comp(m);
  comp.for_each_voxel([=](const cube_handle &voxel) {
    stats->update(voxel.val, chunk);
  });
}

template <typename counted_type, int bucket_size = 1>
struct histogram {
  std::unordered_map<counted_type, size_t> h;
  size_t all = 0;

  void add(const counted_type &observation, size_t times = 1) {
    h[observation / bucket_size * bucket_size] += times;
    all += times;
  }

  void report() {
    std::cout << "hist: " << h << std::endl;
    std::vector<std::pair<size_t, counted_type>> v;
    for (auto x : h) v.emplace_back(std::make_pair(x.second, x.first));
    // I want python now...
    std::sort(v.begin(), v.end());
    std::cout << "sorted (times : val):" << v << std::endl;
  }

  void report_percents() {
    std::cout << "hist: " << h << std::endl;
    std::vector<std::pair<double, counted_type>> v;
    for (auto x : h)
      v.emplace_back(std::make_pair(100.0 * x.second / all, x.first));
    // I want python now...
    std::sort(v.begin(), v.end());
    std::cout << "sorted (percent : val):" << v << std::endl;
  }
};
